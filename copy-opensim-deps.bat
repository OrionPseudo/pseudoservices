@ECHO OFF

if "%1" == "" GOTO ERROR

COPY %1\bin\OpenMetaverse*.dll bin
COPY %1\bin\OpenSim.Data*.dll bin
COPY %1\bin\OpenSim.Framework*.dll bin
COPY %1\bin\OpenSim.Region.Framework.dll bin
COPY %1\bin\OpenSim.Services.*.dll bin
COPY %1\bin\OpenSim.Server.*.dll bin
COPY %1\bin\Mono.Addins*.dll bin
COPY %1\bin\XMLRPC.dll bin

GOTO DONE

:ERROR
ECHO Please specify a directory to copy from.

:DONE
ECHO I'm done.