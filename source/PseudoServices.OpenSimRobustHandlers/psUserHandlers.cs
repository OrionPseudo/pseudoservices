/*
Copyright Contributors, Pseudospace Virtual Worlds

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the OpenSimulator Project nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE DEVELOPERS ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


using System;
using System.Collections.Generic;
using System.Reflection;
using System.Net;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using log4net;
using Nini.Config;

using OpenSim.Framework;
using OpenSim.Services.AuthenticationService;
using OpenSim.Services.AvatarService;
using OpenSim.Services.GridService;
using OpenSim.Services.InventoryService;
using OpenSim.Services.PresenceService;
using OpenSim.Services.UserAccountService;
using OpenSim.Services.Interfaces;

using OpenMetaverse;

using PseudoServices.Types;
using Pseudospace.Types;
using Pseudospace.Networking.HTTP;
using Pseudospace.Utility;

namespace PseudoServices.OpenSimRobustHandlers
{
    public class psUserHandlers : IpsComponent
    {
        private IpsComponentRegistry m_ComponentRegistry;
        private IpsFeedbackController m_Feedback;
        private PasswordAuthenticationService m_AuthService;
        private UserAccountService m_UserService;

        private IpsHttpServer m_HttpServer;

        private bool m_IsInitialized = false;
        private bool m_IsPostInitialized = false;
        
        public string ComponentName { get { return("PseudoServices User Handler"); } }
        public bool Initialized { get { return(m_IsInitialized); } }
        public psComponentInitializationPriority InitPriority { get { return(psComponentInitializationPriority.Last); } }
        public bool IsReplacable { get { return(false); } }
        public bool PostInitialized { get { return(m_IsPostInitialized); } }

        public void Initialize(IpsComponentRegistry ComponentRegistry)
        {
            m_ComponentRegistry = ComponentRegistry;
            m_Feedback = m_ComponentRegistry.GetComponent<IpsFeedbackController>();
            m_HttpServer = m_ComponentRegistry.GetComponent<IpsHttpServer>();

            m_AuthService = (PasswordAuthenticationService)m_ComponentRegistry.GetGenericComponent<IAuthenticationService>();
            m_UserService = (UserAccountService)m_ComponentRegistry.GetGenericComponent<IUserAccountService>();

            m_HttpServer.RegisterPath("/AuthUser/", new psHttpRequestDelegate(HandleAuthUser));
            m_HttpServer.RegisterPath("/SearchUser/", new psHttpRequestDelegate(HandleSearchUser));

            m_IsInitialized = true;
        }

        public void PostInitialize()
        {
            m_IsPostInitialized = true;
        }

        public void HandleAuthUser(HttpListenerContext Context)
        {
            m_Feedback.Notice(ComponentName, "Auth user called.");

            if (Context.Request.HttpMethod == "POST")
            {
                XmlSerializer Serializer = new XmlSerializer(typeof(psUserLoginData));

                psUserLoginData AuthInfo = (psUserLoginData)Serializer.Deserialize(Context.Request.InputStream);

                Context.Request.InputStream.Close();

                psUserAuthResult m_Result = new psUserAuthResult();
                UserAccount Account = m_UserService.GetUserAccount(UUID.Zero, AuthInfo.FirstName, AuthInfo.LastName);

                if(Account != null)
                {
                    m_Result.Token = m_AuthService.Authenticate(Account.PrincipalID, AuthInfo.Password, 60);
                    m_Result.Account = new psUserAccount();

                    m_Result.Account.Email = Account.Email;
                    m_Result.Account.FirstName = Account.FirstName;
                    m_Result.Account.LastName = Account.LastName;
                    m_Result.Account.Title = Account.UserTitle;
                    m_Result.Account.UserID = Account.PrincipalID;
                    m_Result.Account.UserLevel = Account.UserLevel;
                }

                SendResponse<psUserAuthResult>(Context.Response, m_Result);
            }
            else
            {
                SendResponse<string>(Context.Response, "Invalid Method");
            }

            return;
        }

        public void HandleSearchUser(HttpListenerContext Context)
        {
            m_Feedback.Notice(ComponentName, "Search user called.");

            if (Context.Request.HttpMethod == "POST")
            {
                XmlSerializer Serializer = new XmlSerializer(typeof(string));

                string search = (string)Serializer.Deserialize(Context.Request.InputStream);

                Context.Request.InputStream.Close();

                List<psUserAccount> rets = new List<psUserAccount>();
                List<UserAccount> results = m_UserService.GetUserAccounts(UUID.Zero, search);

                foreach(UserAccount U in results)
                {
                    psUserAccount A = new psUserAccount();

                    A.UserID = U.PrincipalID;
                    A.FirstName = U.FirstName;
                    A.LastName = U.LastName;
                    A.Email = U.Email;
                    A.Title = U.UserTitle;
                    A.UserLevel = U.UserLevel;

                    rets.Add(A);
                }

                SendResponse<List<psUserAccount>>(Context.Response, rets);
            }
            else
            {
                SendResponse<string>(Context.Response, "Invalid Method");
            }

            return;
        }

        private void SendResponse<T>(HttpListenerResponse Response, T ReturnObject)
        {
            Response.ContentType = "text/xml";

            MemoryStream Buffer = new MemoryStream();
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Encoding = System.Text.Encoding.UTF8;

            XmlSerializer Serializer = new XmlSerializer(typeof(T));
            XmlWriter XMLOutput = XmlWriter.Create(Buffer, settings);

            Serializer.Serialize(XMLOutput, ReturnObject);
            Response.ContentLength64 = Buffer.Length;

            Response.OutputStream.Write(Buffer.ToArray(), 0, (int)Buffer.Length);
            Response.OutputStream.Close();
            Response.Close();
        }
    }
}

