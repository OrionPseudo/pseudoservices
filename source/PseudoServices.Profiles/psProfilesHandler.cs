/*
Copyright Contributors, Pseudospace Virtual Worlds

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the OpenSimulator Project nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE DEVELOPERS ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

using System;
using System.IO;
using System.Net;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;

using OpenSim.Framework;
using OpenMetaverse;

using Pseudospace.Types;
using Pseudospace.Networking.HTTP;
using PseudoServices.Types;
using Pseudospace.Utility;


namespace PseudoServices.Profiles
{
    public class psProfilesHandler : IpsProfilesHandler
    {
        private IpsComponentRegistry m_ComponentRegistry;
        private IpsFeedbackController m_Feedback;

        private IpsHttpServer m_HttpServer;
        private IpsProfileDatabase m_ProfilesDatabase;

        private bool m_IsInitialized = false;
        private bool m_IsPostInitialized = false;
        
        public string ComponentName { get { return("Offline Messaging Handler"); } }
        public bool Initialized { get { return(m_IsInitialized); } }
        public psComponentInitializationPriority InitPriority { get { return(psComponentInitializationPriority.Last); } }
        public bool IsReplacable { get { return(false); } }
        public bool PostInitialized { get { return(m_IsPostInitialized); } }

        public void Initialize(IpsComponentRegistry ComponentRegistry)
        {
            m_ComponentRegistry = ComponentRegistry;
            m_Feedback = m_ComponentRegistry.GetComponent<IpsFeedbackController>();
            m_HttpServer = m_ComponentRegistry.GetComponent<IpsHttpServer>();
            m_ProfilesDatabase = m_ComponentRegistry.GetComponent<IpsProfileDatabase>();

            m_HttpServer.RegisterPath("/GetClassifieds/", HandleGetClassifieds);
            m_HttpServer.RegisterPath("/SaveClassified/", HandleSaveClassified);
            m_HttpServer.RegisterPath("/DeleteClassified/", HandleDeleteClassified);

            m_IsInitialized = true;
        }

        public void PostInitialize()
        {
            m_IsPostInitialized = true;
        }

        public void HandleGetClassifieds(HttpListenerContext Context)
        {
            if (Context.Request.HttpMethod == "POST")
            {
                XmlSerializer Serializer = new XmlSerializer(typeof(UUID));

                UUID OwnerID = (UUID)Serializer.Deserialize(Context.Request.InputStream);

                Context.Request.InputStream.Close();
          
                SendResponse<List<psProfileClassified>>(Context.Response, m_ProfilesDatabase.GetClassifieds(OwnerID));
            }
            else
            {
                SendResponse<List<psProfileClassified>>(Context.Response, new List<psProfileClassified>());
            }

           
            return;
        }

        public void HandleSaveClassified(HttpListenerContext Context)
        {
            bool Success = false;
            try
            {
                if (Context.Request.HttpMethod == "POST")
                {
                    XmlSerializer Serializer = new XmlSerializer(typeof(GridInstantMessage));

                    psProfileClassified C = (psProfileClassified)Serializer.Deserialize(Context.Request.InputStream);

                    Context.Request.InputStream.Close();
                   
                    Success = m_ProfilesDatabase.SaveClassified(C);
                } 

            } 
            catch (Exception e)
            {
                m_Feedback.Exception(ComponentName, "Error handling save of classified.", e);
            }

            SendResponse<bool>(Context.Response, Success);
        }

        public void HandleDeleteClassified(HttpListenerContext Context)
        {
            bool Success = false;
            try
            {
                if (Context.Request.HttpMethod == "POST")
                {
                    XmlSerializer Serializer = new XmlSerializer(typeof(GridInstantMessage));

                    UUID ID = (UUID)Serializer.Deserialize(Context.Request.InputStream);

                    Context.Request.InputStream.Close();
                   
                    Success = m_ProfilesDatabase.DeleteClassified(ID);
                } 

            } 
            catch (Exception e)
            {
                m_Feedback.Exception(ComponentName, "Error handling delete of classified.", e);
            }

            SendResponse<bool>(Context.Response, Success);
        }

        private void SendResponse<T>(HttpListenerResponse Response, T ReturnObject)
        {
            Response.ContentType = "text/xml";

            MemoryStream Buffer = new MemoryStream();
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Encoding = System.Text.Encoding.UTF8;

            XmlSerializer Serializer = new XmlSerializer(typeof(T));
            XmlWriter XMLOutput = XmlWriter.Create(Buffer, settings);

            Serializer.Serialize(XMLOutput, ReturnObject);
            Response.ContentLength64 = Buffer.Length;

            Response.OutputStream.Write(Buffer.ToArray(), 0, (int)Buffer.Length);
            Response.OutputStream.Close();
            Response.Close();
        }
    }
}

