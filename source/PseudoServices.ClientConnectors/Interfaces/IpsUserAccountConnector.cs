using System;
using System.Collections.Generic;
using Pseudospace.Types;
using PseudoServices.Types;

namespace PseudoServices.ClientConnectors
{
    public interface IpsUserAccountConnector : IpsComponent
    {
        string AuthenticateUser(psUserLoginData LoginData);
        string AuthenticateUser(psUserLoginData LoginData, out psUserAccount Account);
        List<psUserAccount> SearchUsers(string Query);
    }
}

