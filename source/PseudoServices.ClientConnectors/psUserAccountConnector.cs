using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using Pseudospace.Types;
using Pseudospace.Networking;
using PseudoServices.Types;

using OpenSim.Framework;

namespace PseudoServices.ClientConnectors
{
    public class psUserAccountConnector : IpsUserAccountConnector
    {
        private IpsComponentRegistry m_ComponentRegistry;
        private IpsFeedbackController m_Feedback;
        private IpsXMLHttpClient m_XMLClient;

        private bool m_IsInitialized = false;
        private bool m_IsPostInitialized = false;

        private string m_RobustURL = "";
        
        public string ComponentName { get { return("PseudoServices User Authentication Connector"); } }
        public bool Initialized { get { return(m_IsInitialized); } }
        public psComponentInitializationPriority InitPriority { get { return(psComponentInitializationPriority.Last); } }
        public bool IsReplacable { get { return(false); } }
        public bool PostInitialized { get { return(m_IsPostInitialized); } }

        public void Initialize(IpsComponentRegistry ComponentRegistry)
        {
            m_ComponentRegistry = ComponentRegistry;
            m_Feedback = m_ComponentRegistry.GetComponent<IpsFeedbackController>();
            m_XMLClient = m_ComponentRegistry.GetComponent<IpsXMLHttpClient>();

            IpsConfigRegistry Configs = m_ComponentRegistry.GetComponent<IpsConfigRegistry>();
            m_RobustURL = Configs.GetSectionItem<string>("Grid", "RobustURL");


            m_IsInitialized = true;
        }

        public void PostInitialize()
        {
            m_IsPostInitialized = true;
        }

        public string AuthenticateUser(psUserLoginData LoginData)
        {
            psUserAccount Junk;
            return(AuthenticateUser(LoginData, out Junk));
        }

        public string AuthenticateUser(psUserLoginData LoginData, out psUserAccount Account)
        {
            //scramble the password!
            LoginData.Password = Md5Hash(LoginData.Password);

            psUserAuthResult Result = m_XMLClient.MakeSynchronousRequest<psUserLoginData, psUserAuthResult>("POST", m_RobustURL + "AuthUser/", LoginData);
            Account = Result.Account;
            return(Result.Token);
        }

        public List<psUserAccount> SearchUsers(string Query)
        {
            return(m_XMLClient.MakeSynchronousRequest<string, List<psUserAccount>>("POST", m_RobustURL + "SearchUser/", Query));
        }

        private string Md5Hash(string data)
        {
            byte[] dataMd5 = ComputeMD5Hash(data);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < dataMd5.Length; i++)
                sb.AppendFormat("{0:x2}", dataMd5[i]);
            return sb.ToString();
        }

        private byte[] ComputeMD5Hash(string data)
        {
            MD5 md5 = MD5.Create();
            return md5.ComputeHash(Encoding.Default.GetBytes(data));
        }
    }
}

