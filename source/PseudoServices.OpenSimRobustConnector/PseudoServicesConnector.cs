/*
Copyright Contributors, Pseudospace Virtual Worlds

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the OpenSimulator Project nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE DEVELOPERS ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


using System;
using System.Collections.Generic;
using System.Reflection;
using log4net;
using Nini.Config;
using OpenSim.Server.Base;
using OpenSim.Framework.Servers.HttpServer;
using OpenSim.Server.Handlers.Base;

using OpenSim.Services.AuthenticationService;
using OpenSim.Services.AvatarService;
using OpenSim.Services.GridService;
using OpenSim.Services.InventoryService;
using OpenSim.Services.PresenceService;
using OpenSim.Services.UserAccountService;
using OpenSim.Services.Interfaces;

using PseudoServices.Types;
using Pseudospace.Types;
using Pseudospace.Networking.HTTP;
using Pseudospace.Utility;
using PseudoServices.OpenSimRobustHandlers;

namespace PseudoServices.OpenSimRobustConnector
{
    public class PseudoServicesConnector : ServiceConnector
    {
        private static readonly ILog m_log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private IpsFeedbackController m_FeedbackController = new FeedbackController();
        private psComponentRegistry m_ComponentRegistry;




        private const string LogMarker = "PseudoServices";

        public PseudoServicesConnector(IConfigSource config, IHttpServer server, string configName) :
            base(config, server, configName)
        {

            IConfig m_Config = config.Configs ["PseudoServices"];

            if (m_Config == null)
            {
                throw new Exception(String.Format("No section {0} in config file", "PseudoServices"));
            }

            m_FeedbackController.Startup(m_log);
            m_FeedbackController.Notice(LogMarker, "Initializing PseudoServices ROBUST Connector.");
            m_ComponentRegistry = new psComponentRegistry(m_FeedbackController);

            LoadOSComponents(config);

            //First level components

            //Middle level components
            IpsHttpServer m_Server = new psHttpServer();
            m_ComponentRegistry.RegisterComponent<IpsHttpServer>(m_Server);

            //Last level components
            m_ComponentRegistry.RegisterComponent<psUserHandlers>(new psUserHandlers());

            #region First level init
            m_ComponentRegistry.Initialize(psComponentInitializationPriority.First);

            #endregion

            #region Middle level init
            m_ComponentRegistry.Initialize(psComponentInitializationPriority.Middle);

            //setup http server
            m_Server.BaseURL = m_Config.GetString("BaseURL");
            m_FeedbackController.Notice(LogMarker, "Listening on: " + m_Server.BaseURL);

            #endregion

            #region Last level init

            m_ComponentRegistry.Initialize(psComponentInitializationPriority.Last);

            #endregion



            #region Post Init
            m_ComponentRegistry.PostInitialize(psComponentInitializationPriority.First);
            m_ComponentRegistry.PostInitialize(psComponentInitializationPriority.Middle);
            m_ComponentRegistry.PostInitialize(psComponentInitializationPriority.Last);

            #endregion
        }

        private void LoadOSComponents(IConfigSource Config)
        {
            m_ComponentRegistry.RegisterGenericComponent<IAuthenticationService>(new PasswordAuthenticationService(Config));
            m_ComponentRegistry.RegisterGenericComponent<IAvatarService>(new AvatarService(Config));
            m_ComponentRegistry.RegisterGenericComponent<IGridService>(new GridService(Config));
            m_ComponentRegistry.RegisterGenericComponent<IInventoryService>(new XInventoryService(Config));
            m_ComponentRegistry.RegisterGenericComponent<IPresenceService>(new PresenceService(Config));
            m_ComponentRegistry.RegisterGenericComponent<IUserAccountService>(new UserAccountService(Config));
        }
    }
}
