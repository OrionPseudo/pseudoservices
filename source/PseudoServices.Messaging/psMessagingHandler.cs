/*
Copyright Contributors, Pseudospace Virtual Worlds

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the OpenSimulator Project nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE DEVELOPERS ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

using System;
using System.IO;
using System.Net;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;

using OpenSim.Framework;
using OpenMetaverse;

using Pseudospace.Types;
using Pseudospace.Networking.HTTP;
using PseudoServices.Types;
using Pseudospace.Utility;


namespace PseudoServices.Messaging
{
    public class psMessagingHandler : IpsMessagingHandler
    {
        private IpsComponentRegistry m_ComponentRegistry;
        private IpsFeedbackController m_Feedback;

        private IpsHttpServer m_HttpServer;
        private IpsMessagingDatabase m_MessageDatabase;

        private bool m_IsInitialized = false;
        private bool m_IsPostInitialized = false;
        
        public string ComponentName { get { return("Offline Messaging Handler"); } }
        public bool Initialized { get { return(m_IsInitialized); } }
        public psComponentInitializationPriority InitPriority { get { return(psComponentInitializationPriority.Last); } }
        public bool IsReplacable { get { return(false); } }
        public bool PostInitialized { get { return(m_IsPostInitialized); } }

        public void Initialize(IpsComponentRegistry ComponentRegistry)
        {
            m_ComponentRegistry = ComponentRegistry;
            m_Feedback = m_ComponentRegistry.GetComponent<IpsFeedbackController>();
            m_HttpServer = m_ComponentRegistry.GetComponent<IpsHttpServer>();
            m_MessageDatabase = m_ComponentRegistry.GetComponent<IpsMessagingDatabase>();

            m_HttpServer.RegisterPath("/RetrieveMessages/", HandleGetMessages);
            m_HttpServer.RegisterPath("/SaveMessage/", HandleSaveMessage);

            m_IsInitialized = true;
        }

        public void PostInitialize()
        {
            m_IsPostInitialized = true;
        }

        public void HandleGetMessages(HttpListenerContext Context)
        {
            m_Feedback.Notice(ComponentName, "Get messages called.");

            if (Context.Request.HttpMethod == "POST")
            {
                XmlSerializer Serializer = new XmlSerializer(typeof(UUID));

                UUID ToID = (UUID)Serializer.Deserialize(Context.Request.InputStream);

                Context.Request.InputStream.Close();

                List<GridInstantMessage> Offlines = m_MessageDatabase.GetMessages(ToID);

                m_Feedback.Notice(ComponentName, "Compiling message array for user " + ToID.ToString());

                if(Offlines.Count >= 24)
                {
                    Offlines.RemoveRange(23, Offlines.Count - 23);
                    Offlines.Add(new GridInstantMessage(null, UUID.Zero, "PseudoServices Offline Messaging", ToID, (byte)InstantMessageDialog.MessageFromObject, "Too many messages, delivery has been capped. :(", true, Vector3.Zero));
                    m_Feedback.Notice(ComponentName, "Too many messages - capping delivery.");
                }
                else if (Offlines.Count <= 24)
                {
                    Offlines.Add(new GridInstantMessage(null, UUID.Zero, "Pseudospace", ToID, (byte)InstantMessageDialog.MessageFromObject, "Welcome to wherever your imagination may take you!", true, Vector3.Zero));
                }

                m_Feedback.Notice(ComponentName, "Finished compiling message array for user " + ToID.ToString() + " (" + Offlines.Count.ToString() + " messages) Purging messages and sending.");
                m_MessageDatabase.PurgeMessages(ToID);

                SendResponse<List<GridInstantMessage>>(Context.Response, Offlines);
            }
            else
            {
                SendResponse<string>(Context.Response, "404 BAD");
            }

           
            return;
        }

        public void HandleSaveMessage(HttpListenerContext Context)
        {
            m_Feedback.Notice(ComponentName, "Save message called.");
            bool Success = false;
            try
            {
                if (Context.Request.HttpMethod == "POST")
                {
                    XmlSerializer Serializer = new XmlSerializer(typeof(GridInstantMessage));

                    GridInstantMessage IM = (GridInstantMessage)Serializer.Deserialize(Context.Request.InputStream);

                    Context.Request.InputStream.Close();
                   
                    m_Feedback.Notice(ComponentName, "Saving offline message from: " + IM.fromAgentID.ToString() + " (" + IM.fromAgentName + ")" + " to " + IM.toAgentID.ToString());
                
                    //save to database here!
                    Success = m_MessageDatabase.SaveMessage(IM);
                } 

            } 
            catch (Exception e)
            {
                m_Feedback.Exception(ComponentName, "Error handling save of message.", e);
            }

            SendResponse<bool>(Context.Response, Success);
        }

        private void SendResponse<T>(HttpListenerResponse Response, T ReturnObject)
        {
            Response.ContentType = "text/xml";

            MemoryStream Buffer = new MemoryStream();
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Encoding = System.Text.Encoding.UTF8;

            XmlSerializer Serializer = new XmlSerializer(typeof(T));
            XmlWriter XMLOutput = XmlWriter.Create(Buffer, settings);

            Serializer.Serialize(XMLOutput, ReturnObject);
            Response.ContentLength64 = Buffer.Length;

            Response.OutputStream.Write(Buffer.ToArray(), 0, (int)Buffer.Length);
            Response.OutputStream.Close();
            Response.Close();
        }
    }
}

