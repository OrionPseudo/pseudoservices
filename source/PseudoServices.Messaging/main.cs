/*
Copyright Contributors, Pseudospace Virtual Worlds

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the OpenSimulator Project nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE DEVELOPERS ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

using System;

using Pseudospace.ProgramConsole;
using Pseudospace.Networking.HTTP;
using Pseudospace.Types;

using log4net;
using Nini.Config;

using PseudoServices.Types;

namespace PseudoServices.Messaging
{
	public class Program
	{
		private static ILog m_SystemLog;

        private static IpsFeedbackController m_FeedbackController = new FeedbackController();
        private static psComponentRegistry m_ComponentRegistry;
        private static IpsDLLLoader m_DLLLoader = new psDLLLoader();
		
        private static IpsConfigRegistry m_ConfigRegistry = new psConfigRegistry();

		private static IpsConsole m_Console = new psConsole();
		private static IpsHttpServer m_Server = new psHttpServer();

        private static IpsMessagingDatabase m_MessagingDatabase;
        private static psMessagingHandler m_MessageHandler = new psMessagingHandler();

		public static void Main(string[] args)
		{
            #region Initial setup
			//setup logging and initialize base components
			log4net.Config.XmlConfigurator.Configure(); 
            m_SystemLog = log4net.LogManager.GetLogger( typeof( Program ) );
            m_FeedbackController.Startup(m_SystemLog);
            m_ComponentRegistry = new psComponentRegistry(m_FeedbackController);

            //register components into the component registry
            m_FeedbackController.Notice("INIT", "Registering components.");

            //First level components
            m_ComponentRegistry.RegisterComponent<IpsConfigRegistry>(m_ConfigRegistry);
            m_ComponentRegistry.RegisterComponent<IpsDLLLoader>(m_DLLLoader);

            //Middle level components
            m_ComponentRegistry.RegisterComponent<IpsConsole>(m_Console);
            m_ComponentRegistry.RegisterComponent<IpsHttpServer>(m_Server);

            //Last level components
            m_ComponentRegistry.RegisterComponent<psMessagingHandler>(m_MessageHandler);

            #endregion

            #region First level init
            m_ComponentRegistry.Initialize(psComponentInitializationPriority.First);

            //read and assemble configurations
            LoadConfigurations("Messaging.ini");

            #endregion

            #region Middle level init
            m_ComponentRegistry.Initialize(psComponentInitializationPriority.Middle);

			//setup console
			m_Console.AddHandler(new ConsoleShutdown(m_ComponentRegistry));
			m_Console.AddHandler(new ServerStats(m_ComponentRegistry));

			//setup http server
            m_Server.BaseURL = m_ConfigRegistry.GetSectionItem<string>("Network", "BaseURL");

            //register and setup database
            string DatabaseConnector = m_ConfigRegistry.GetSectionItem<string>("Database", "Connector");
            m_MessagingDatabase = m_DLLLoader.LoadDLL<IpsMessagingDatabase>(DatabaseConnector, "psMessagingDatabase", new object[0]);
            m_ComponentRegistry.RegisterComponent<IpsMessagingDatabase>(m_MessagingDatabase);

            #endregion

            #region Last level init
            m_ComponentRegistry.Initialize(psComponentInitializationPriority.Last);

            #endregion

            #region Post Init
            m_ComponentRegistry.PostInitialize(psComponentInitializationPriority.First);
            m_ComponentRegistry.PostInitialize(psComponentInitializationPriority.Middle);
            m_ComponentRegistry.PostInitialize(psComponentInitializationPriority.Last);

            #endregion
			//all systems go!
            m_FeedbackController.Notice("INIT", "Begin run at " + DateTime.Now.ToLongDateString() + " @ " + DateTime.Now.ToShortTimeString());
			m_FeedbackController.Notice("WELCOME", "Wherever your imagination may take you!");
		}

        private static void LoadConfigurations(string INIFileName)
        {
            m_FeedbackController.Notice("INIT", "Loading configurations.");
            m_FeedbackController.Bell();

            IConfigSource Config = new IniConfigSource(INIFileName);

            foreach (IConfig IC in Config.Configs)
            {
                m_ConfigRegistry.AddSection(IC.Name);
                string[] Keys = IC.GetKeys();

                foreach(string S in Keys)
                {
                    m_ConfigRegistry.AddOrUpdateSectionItem(IC.Name, S, IC.Get(S));
                }

            }
        }

		public static void Shutdown()
		{
			m_FeedbackController.Notice("INIT", "Shutdown has been initiated.");
			m_Console.Stop();
			m_Server.Stop();
			m_FeedbackController.Notice("INIT", "Run has been terminated.");
		}
	}

	public class ServerStats : psConsoleCommand
	{	
		public ServerStats(IpsComponentRegistry _Registry) : base(_Registry)
		{
			base.CommandTrigger = "stats";
			base.CommandHelp = "stats";
			base.CommandDetails = "Displays http server stats.";
		}

		public override string RunCommand(string[] CommandParameters)
		{
			IpsHttpServer Server = Registry.GetComponent<IpsHttpServer>();
			string Output = "";
			Output += "Active threads: " + Server.ActiveThreads.ToString() + "\n";
			Output += "Threads in use: " + Server.ThreadsInUse.ToString() + "\n";
			Output += "Post requests: " + Server.PostRequests.ToString() + "\n";
			Output += "Get requests: " + Server.GetRequests.ToString() + "\n";
			Output += "Delete request: " + Server.DeleteRequests.ToString() + "\n";
			Output += "Total requests processed: " + Server.RequestsProcessed.ToString() + "\n";

			return(Output);
		}
	}

	public class ConsoleShutdown : psConsoleCommand
	{	
		public ConsoleShutdown(IpsComponentRegistry _Registry) : base(_Registry)
		{
			base.CommandTrigger = "shutdown";
			base.CommandHelp = "shutdown";
			base.CommandDetails = "Terminates system run.";
		}

		public override string RunCommand(string[] CommandParameters)
		{
			Program.Shutdown();
			return("");
		}
	}	
}

