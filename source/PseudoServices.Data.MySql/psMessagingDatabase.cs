/*
Copyright Contributors, Pseudospace Virtual Worlds

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the OpenSimulator Project nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE DEVELOPERS ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

using System;
using System.Data;
using System.Collections.Generic;
using PseudoServices.Types;
using Pseudospace.Types;
using Pseudospace.Utility;
using MySql.Data.MySqlClient;
using OpenSim.Framework;
using OpenMetaverse;

namespace PseudoServices.Data.MySql
{
    public class psMessagingDatabase : psMySqlDatabase, IpsMessagingDatabase
    {
        /*CREATE TABLE IF NOT EXISTS `messages` (
 `toAgentID` varchar(36) NOT NULL,
 `fromAgentID` varchar(36) NOT NULL,
 `fromAgentName` varchar(128),
 `dialog` tinyint(4) NOT NULL,
 `gromGroup` tinyint(1) NOT NULL,
 `message` text NOT NULL,
 `imSessionID` varchar(36) NOT NULL,
 `offline` tinyint(1) NOT NULL,
 `position` varchar(20),
 `binaryBucket` blob,
 `parentEstateID` tinyint(4),
 `regionID` varchar(36),
 `timestamp` integer,
  KEY `toAgentID` (`toAgentID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;*/

        private const string sql_SaveMessage = "insert into messages(toAgentID, fromAgentID, fromAgentName, dialog, fromGroup, message, " +
                                                       "imSessionID, offline, position, binaryBucket, parentEstateID, regionID, timestamp)" +
                                                       "values(?ToID, ?FromID, ?FromName, ?Dialog, ?FromGroup, ?Message, ?SessionID, ?Offline, ?Position," +
                                                       "?BinaryBucket, ?ParentEstate, ?RegionID, ?Timestamp);";
        private const string sql_GetMessages = "select * from messages where toAgentID = ?To order by timestamp asc;";
        private const string sql_PurgeMessages = "delete from messages where toAgentID = ?Owner;";

        private bool m_Initialized = false;
        private bool m_PostInitialized = false;

        public string ComponentName { get { return("Messaging Database"); } }
        public bool Initialized { get { return(m_Initialized); } }
        public psComponentInitializationPriority InitPriority { get { return(psComponentInitializationPriority.Last); } }
        public bool IsReplacable { get { return(false); } }
        public bool PostInitialized { get { return(m_PostInitialized); } }

        public void Initialize(IpsComponentRegistry ComponentRegistry)
        {
            base.Initialize(ComponentRegistry, ComponentName);
            base.ConnectionString = m_Config.GetSectionItem<string>("MySQL Data Connector", "ConnectionString");
            m_Initialized = true;
        }

        public void PostInitialize()
        {
            base.CheckMigrations(GetType().Assembly, "messaging");
            m_PostInitialized = true;
        }

        public List<GridInstantMessage> GetMessages(UUID User)
        {
            MySqlCommand m_Command = new MySqlCommand(sql_GetMessages);
            
            List<GridInstantMessage> rets = new List<GridInstantMessage>();
            
            m_Command.Parameters.AddWithValue("?To", User.ToString());
                        
            DataTable result = base.RunQuery(m_Command);
            
            foreach(DataRow r in result.Rows)
            {
                GridInstantMessage GM = new GridInstantMessage();
                GM.toAgentID = new Guid(Convert.ToString(r[0]));
                GM.fromAgentID = new Guid(Convert.ToString(r[1]));
                GM.fromAgentName = (string)r[2];
                GM.dialog = Convert.ToByte(r[3]);
                GM.fromGroup = psUtility.IntToBool(Convert.ToInt32(r[4]));
                GM.message = (string)r[5];
                GM.imSessionID = new Guid(Convert.ToString(r[6]));
                GM.offline = Convert.ToByte(r[7]);
                GM.Position = Vector3.Parse((string)r[8]);
                GM.binaryBucket = (byte[])r[9];
                GM.ParentEstateID = Convert.ToUInt32(r[10]);
                GM.RegionID = new Guid(Convert.ToString(r[11]));
                GM.timestamp = Convert.ToUInt32(r[12]);

                rets.Add(GM);   
            }
            
            return(rets);
        }

        public bool PurgeMessages(UUID User)
        {
            MySqlCommand m_Command = new MySqlCommand(sql_PurgeMessages);
            
            m_Command.Parameters.AddWithValue("?Owner", User.ToString());

            return(base.RunAction(m_Command) > -1);
        }

        public bool SaveMessage(GridInstantMessage Message)
        {
            MySqlCommand m_Command = new MySqlCommand(sql_SaveMessage);
               
            m_Command.Parameters.AddWithValue("?ToID", Message.toAgentID);
            m_Command.Parameters.AddWithValue("?FromID", Message.fromAgentID);
            m_Command.Parameters.AddWithValue("?FromName", Message.fromAgentName);
            m_Command.Parameters.AddWithValue("?Dialog", Message.dialog);
            m_Command.Parameters.AddWithValue("?FromGroup", Message.fromGroup);
            m_Command.Parameters.AddWithValue("?Message", Message.message);
            m_Command.Parameters.AddWithValue("?SessionID", Message.imSessionID);
            m_Command.Parameters.AddWithValue("?Offline", Message.offline);
            m_Command.Parameters.AddWithValue("?Position", Message.Position);
            m_Command.Parameters.AddWithValue("?BinaryBucket", Message.binaryBucket);
            m_Command.Parameters.AddWithValue("?ParentEstate", Message.ParentEstateID);
            m_Command.Parameters.AddWithValue("?RegionID", Message.RegionID);
            m_Command.Parameters.AddWithValue("?Timestamp", Message.timestamp);
            
            return(base.RunAction(m_Command) > -1);
        }
    }
}

