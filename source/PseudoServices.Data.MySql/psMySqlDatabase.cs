/*
Copyright Contributors, Pseudospace Virtual Worlds

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the OpenSimulator Project nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE DEVELOPERS ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

using System;
using System.Data;
using System.Data.Common;
using System.Xml;

using Pseudospace.Types;
using Pseudospace.DatabaseConnectors;
using PseudoServices.Types;

using MySql.Data.MySqlClient;

namespace PseudoServices.Data.MySql
{
    public class psMySqlDatabase : psDatabaseConnectorBase<MySqlCommand>
    {
        protected IpsComponentRegistry m_ComponentRegistry;
        protected IpsFeedbackController m_Feedback;
        protected IpsConfigRegistry m_Config;

        protected string LogMarker;
        protected string ConnectionString;

        public override bool Initialize(IpsComponentRegistry ComponentRegistry, string _LogMarker)
        {
            m_ComponentRegistry = ComponentRegistry;
            m_Feedback = m_ComponentRegistry.GetComponent<IpsFeedbackController>();
            m_Config = m_ComponentRegistry.GetComponent<IpsConfigRegistry>();
            LogMarker = _LogMarker;

            return(true);
        }

        protected override bool CheckMigrations(System.Reflection.Assembly Assembly, string type)
        {
            try
            {
                MySqlConnection InitialConnection = new MySqlConnection(ConnectionString);
                InitialConnection.Open();
                
                Migration m = new Migration(m_ComponentRegistry, InitialConnection, Assembly, type);
                m.Update();

                InitialConnection.Close();
                InitialConnection.Dispose();
                return(true);
            }
            catch (Exception e)
            {
                m_Feedback.Exception(LogMarker, "An error has occured while initializing the database!", e);
                return(false);
            }   
        }

        protected override int RunAction(MySqlCommand Query)
        {
            MySqlConnection m_QueryConnection = new MySqlConnection(ConnectionString);
            Query.Connection = m_QueryConnection;
            int Rows = 0;
            try
            {
                m_QueryConnection.Open();
                Rows = Query.ExecuteNonQuery();
            }
            catch (MySqlException e)
            {
                m_Feedback.Exception(LogMarker, "An error has occured while attempting to perform an action query:", e);
                Rows = 0;
            }
            finally
            {
                m_QueryConnection.Close();
                Query.Dispose();
                m_QueryConnection.Dispose();
            }
            return (Rows);
        }

        protected override DataTable RunQuery(MySqlCommand Query)
        {
            MySqlConnection m_QueryConnection = new MySqlConnection(ConnectionString);
            MySqlDataAdapter m_Adapter;
            DataTable r = new DataTable();

            Query.Connection = m_QueryConnection;

            m_Adapter = new MySqlDataAdapter(Query);
            try
            {
                m_QueryConnection.Open();
                m_Adapter.Fill(r);
            }
            catch (MySqlException e)
            {
                m_Feedback.Exception(LogMarker, "An error has occured while attempting to perform a query:", e);
            }
            finally
            {
                m_QueryConnection.Close();
                m_Adapter.Dispose();
                m_QueryConnection.Dispose();
            }
            return (r);
        }
    }
}