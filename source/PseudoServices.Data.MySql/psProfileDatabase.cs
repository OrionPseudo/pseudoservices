/*
Copyright Contributors, Pseudospace Virtual Worlds

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the OpenSimulator Project nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE DEVELOPERS ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

using System;
using System.Data;
using System.Collections.Generic;
using PseudoServices.Types;
using Pseudospace.Types;
using Pseudospace.Utility;
using MySql.Data.MySqlClient;
using OpenSim.Framework;
using OpenMetaverse;

namespace PseudoServices.Data.MySql
{
    public class psProfileDatabase : psMySqlDatabase, IpsProfileDatabase
    {
        private const string sql_GetClassifieds = "select * from profile_classifieds where creatoruuid = ?UUID;";
        private const string sql_SaveClassified = "replace into profile_classifieds(classifieduuid, creatoruuid, creationdate, expirationdate, category, name, description," +
                                                  "parceluuid, parentstate, snapshotuuid, simname, posglobal, parcelname, classifiedflags, priceforlisting) " +
                                                  "values (?classifiedID, ?creatorID, ?creationDate, ?expirationDate, ?category, ?name, ?description, ?parcelID, ?estateID," +
                                                  "?snapshotID, ?simName, ?position, ?parcelname, ?flags, ?price);";
        private const string sql_DeleteClassified = "delete from profile_classifieds where classifieduuid = ?ID;";

        /* `pickuuid` varchar(36) NOT NULL,
         `creatoruuid` varchar(36) NOT NULL, 
         `toppick` enum('true','false') NOT NULL,
         `parceluuid` varchar(36) NOT NULL, 
         `name` varchar(255) NOT NULL,
         `description` text NOT NULL,
         `snapshotuuid` varchar(36) NOT NULL,
         `user` varchar(255) NOT NULL,
         `originalname` varchar(255) NOT NULL,
         `simname` varchar(255) NOT NULL,
         `posglobal` varchar(255) NOT NULL,
         `sortorder` int(2) NOT NULL,
         `enabled` enum('true','false') NOT NULL,*/

        private const string sql_GetPicks = "select * from profile_picks where creatoruuid = ?CreatorID;";
        private const string sql_GetPick = "select * from profile_picks where pickuuid = ?PickID;";
        private const string sql_SavePick = "replace into profile_picks(pickuuid, creatoruuid, toppick, parceluuid, name, description, snapshotuuid, user, originalname, simname," +
                                            "posglobal, sortorder, enabled) values (?pickID, ?creatorID, ?topPick, ?parcelID, ?name, ?desc, ?snapshotID, ?user, ?originalName," +
                                            "?simName, ?posGlobal, ?sortOrder, ?enabled);";
        private const string sql_DeletePick = "delete from profile_picks where pickuuid = ?PickID;";

        private bool m_Initialized = false;
        private bool m_PostInitialized = false;

        public string ComponentName { get { return("Profiles Database"); } }
        public bool Initialized { get { return(m_Initialized); } }
        public psComponentInitializationPriority InitPriority { get { return(psComponentInitializationPriority.Last); } }
        public bool IsReplacable { get { return(false); } }
        public bool PostInitialized { get { return(m_PostInitialized); } }

        public void Initialize(IpsComponentRegistry ComponentRegistry)
        {
            base.Initialize(ComponentRegistry, ComponentName);
            base.ConnectionString = m_Config.GetSectionItem<string>("MySQL Data Connector", "ConnectionString");
            m_Initialized = true;
        }

        public void PostInitialize()
        {
            base.CheckMigrations(GetType().Assembly, "profiles");
            m_PostInitialized = true;
        }

        public List<psProfileClassified> GetClassifieds(UUID Owner)
        {
            MySqlCommand m_Command = new MySqlCommand(sql_GetClassifieds);
            
            List<psProfileClassified> rets = new List<psProfileClassified>();
            
            m_Command.Parameters.AddWithValue("?UUID", Owner.ToString());
                        
            DataTable result = base.RunQuery(m_Command);
            
            foreach(DataRow r in result.Rows)
            {
                psProfileClassified C = new psProfileClassified();

                /*
                 `classifieduuid` char(36) NOT NULL,
                 `creatoruuid` char(36) NOT NULL, 
                 `creationdate` int(20) NOT NULL,
                 `expirationdate` int(20) NOT NULL, 
                 `category` varchar(20) NOT NULL,
                 `name` varchar(255) NOT NULL, 
                 `description` text NOT NULL,
                 `parceluuid` char(36) NOT NULL, 
                 `parentestate` int(11) NOT NULL,
                 `snapshotuuid` char(36) NOT NULL, 
                 `simname` varchar(255) NOT NULL,
                 `posglobal` varchar(255) NOT NULL, 
                 `parcelname` varchar(255) NOT NULL,
                 `classifiedflags` int(8) NOT NULL, 
                 `priceforlisting` int(5) NOT NULL,
                */

                C.classifieduuid = new UUID(Convert.ToString(r[0]));
                C.creatoruuid = new UUID(Convert.ToString(r[1]));
                C.creationdate = Convert.ToDouble(r[2]);
                C.expirationdate = Convert.ToDouble(r[3]);
                C.category = Convert.ToUInt32(r[4]);
                C.name = Convert.ToString(r[5]);
                C.description = Convert.ToString(r[6]);
                C.parceluuid = new UUID(Convert.ToString(r[7]));
                C.parentestate = Convert.ToUInt32(r[8]);
                C.snapshotuuid = new UUID(Convert.ToString(r[9]));
                C.simname = Convert.ToString(r[10]);
                C.globalPos = Vector3.Parse(Convert.ToString(r[11]));
                C.parcelname = Convert.ToString(r[12]);
                C.classifiedflags = Convert.ToByte(r[13]);
                C.priceforlisting = Convert.ToInt32(r[14]);

                rets.Add(C);   
            }
            
            return(rets);
        }

        public bool SaveClassified(psProfileClassified c)
        {
            MySqlCommand m_Command = new MySqlCommand(sql_SaveClassified);

            m_Command.Parameters.AddWithValue("?classifiedID", c.classifieduuid.ToString());
            m_Command.Parameters.AddWithValue("?creatorID", c.creatoruuid.ToString());
            m_Command.Parameters.AddWithValue("?creationDate", c.creationdate);
            m_Command.Parameters.AddWithValue("?expirationDate", c.expirationdate);
            m_Command.Parameters.AddWithValue("?category", c.category);
            m_Command.Parameters.AddWithValue("?name", c.name);
            m_Command.Parameters.AddWithValue("?description", c.description);
            m_Command.Parameters.AddWithValue("?parcelID", c.parceluuid.ToString());
            m_Command.Parameters.AddWithValue("?estateID", c.parentestate.ToString());
            m_Command.Parameters.AddWithValue("?snapshotID", c.snapshotuuid.ToString());
            m_Command.Parameters.AddWithValue("?simName", c.simname);
            m_Command.Parameters.AddWithValue("?position", c.globalPos.ToString());
            m_Command.Parameters.AddWithValue("?parcelname", c.parcelname);
            m_Command.Parameters.AddWithValue("?flags", c.classifiedflags);
            m_Command.Parameters.AddWithValue("?price", c.priceforlisting);

            return(base.RunAction(m_Command) > -1);
        }

        public bool DeleteClassified(UUID ClassifiedID)
        {
            MySqlCommand m_Command = new MySqlCommand(sql_DeleteClassified);
            
            m_Command.Parameters.AddWithValue("?ID", ClassifiedID.ToString());

            return(base.RunAction(m_Command) > -1);
        }

        public List<psProfilePick> GetPicks(UUID Owner)
        {
            MySqlCommand m_Command = new MySqlCommand(sql_GetPicks);
            
            List<psProfilePick> rets = new List<psProfilePick>();
            
            m_Command.Parameters.AddWithValue("?CreatorID", Owner.ToString());
                        
            DataTable result = base.RunQuery(m_Command);
            
            foreach(DataRow r in result.Rows)
            {
                psProfilePick P = new psProfilePick();
        /* `pickuuid` varchar(36) NOT NULL,
         `creatoruuid` varchar(36) NOT NULL, 
         `toppick` enum('true','false') NOT NULL,
         `parceluuid` varchar(36) NOT NULL, 
         `name` varchar(255) NOT NULL,
         `description` text NOT NULL,
         `snapshotuuid` varchar(36) NOT NULL,
         `user` varchar(255) NOT NULL,
         `originalname` varchar(255) NOT NULL,
         `simname` varchar(255) NOT NULL,
         `posglobal` varchar(255) NOT NULL,
         `sortorder` int(2) NOT NULL,
         `enabled` enum('true','false') NOT NULL,*/

                P.pickuuid = new UUID(Convert.ToString(r[0]));
                P.creatoruuid = new UUID(Convert.ToString(r[1]));
                //P.toppick = r[2];
                P.parceluuid = new UUID(Convert.ToString(r[3]));
                P.name = Convert.ToString(r[4]);
                P.description = Convert.ToString(r[5]);
                P.snapshotuuid = new UUID(Convert.ToString(r[6]));


               /* C.classifieduuid = new UUID(Convert.ToString(r[0]));
                C.creatoruuid = new UUID(Convert.ToString(r[1]));
                C.creationdate = Convert.ToDouble(r[2]);
                C.expirationdate = Convert.ToDouble(r[3]);
                C.category = Convert.ToString(r[4]);
                C.name = Convert.ToString(r[5]);
                C.description = Convert.ToString(r[6]);
                C.parceluuid = new UUID(Convert.ToString(r[7]));
                C.parentestate = Convert.ToUInt32(r[8]);
                C.snapshotuuid = new UUID(Convert.ToString(r[9]));
                C.simname = Convert.ToString(r[10]);
                C.globalPos = Vector3.Parse(Convert.ToString(r[11]));
                C.parcelname = Convert.ToString(r[12]);
                C.classifiedflags = Convert.ToByte(r[13]);
                C.priceforlisting = Convert.ToInt32(r[14]);

                rets.Add(C);   */
            }
            
            return(rets);
        }
    }
}

