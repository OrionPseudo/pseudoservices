#! /bin/sh

if [ $# -eq 0 ]
  then
echo "Please specify a directory to copy from"
    exit
fi

if [ ! -d "bin" ]; then
mkdir bin
fi

cp -r $1/bin/OpenMetaverse*.dll bin
cp -r $1/bin/OpenSim.Data*.dll bin
cp -r $1/bin/OpenSim.Framework*.dll bin
cp -r $1/bin/OpenSim.Region.Framework.dll bin
cp -r $1/bin/OpenSim.Services.*.dll bin
cp -r $1/bin/OpenSim.Server.*.dll bin
cp -r $1/bin/Mono.Addins*.dll bin
cp -r $1/bin/XMLRPC.dll bin
